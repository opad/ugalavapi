from django.shortcuts import render
from django.conf import settings
from django.urls import reverse
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Payments
from classifieds.models import Classified
from django.db.models.functions import Now
from datetime import datetime, timedelta
from pyfcm import FCMNotification

import stripe

stripe.api_key = settings.STRIPE_PRIVATE_KEY

def index(request):
    return render(request, 'payments/index.html')

def thanks(request):
    payment = Payments.objects.get(sessionId=request.GET['session_id'])

    payment.status = 'Success',
    payment.save()
    classfied = Classified.objects.filter(pk=payment.itemId).first()
    classfied.is_active = 'Y'
    classfied.is_featured = 'Y'
    classfied.expires_on = datetime.now() + timedelta(days=30)
    classfied.save()
    push_service = FCMNotification(api_key="AAAAsF3vcqw:APA91bG6Zwu9KDQrWIIa2AI9DgSuplauwE24q5IZp8syAF4ipLyR9JtybmeMKc5D8EslpK9UGa615hBjPY1cKTraatdey2x1_Zk1TIbWU_9XtIUtwzAUnqaaaRJrmmaLfOcxUJr8kdtI")
    result = push_service.notify_topic_subscribers(topic_name="classifieds", message_body=classfied.title, message_title='UGLAV Forum')
    
    return render(request, 'payments/thanks.html')

def cancel(request):
    return render(request, 'payments/failed.html')

@csrf_exempt
def checkout(request):
    # get item in check for verification
    itemId = request.GET["itemId"]
    classified = Classified.objects.filter(pk=itemId).first()
    product = request.GET["product"]
    price = 'price_1HoBP9CYHZJHJezoxin2Czwj'
    if product == '2':
        price = 'price_1HoBQHCYHZJHJezoPzTpD9wq'
        
    #if category is Death Announcement/2 then choose freemium
    if classified.category == '2':
        price = 'price_1HoBPfCYHZJHJezoA4hMdLOM'

    session = stripe.checkout.Session.create(
        payment_method_types=['card'],
        line_items=[{
            'price': price,
            'quantity': 1,
        }],
        mode='payment',
        success_url=request.build_absolute_uri(reverse('thanks')) + '?session_id={CHECKOUT_SESSION_ID}',
        cancel_url=request.build_absolute_uri(reverse('cancel')),
    )
    #add the payment to payments
    payment = Payments.objects.create(
        sessionId=session.id,
        itemId=request.GET["itemId"],
        amount=1.0,
        category='Classifieds',
        status='Pending'
    )

    return JsonResponse({
        'session_id' : session.id,
        'stripe_public_key' : settings.STRIPE_PUBLIC_KEY
    })

@csrf_exempt
def stripe_webhook(request):

    # You can find your endpoint's secret in your webhook settings
    endpoint_secret = 'whsec_Xj8wBk2qiUcjDEmYu5kfKkOrJCJ5UUjW'

    payload = request.body
    sig_header = request.META['HTTP_STRIPE_SIGNATURE']
    event = None

    try:
        event = stripe.Webhook.construct_event(
            payload, sig_header, endpoint_secret
        )
    except ValueError as e:
        # Invalid payload
        return HttpResponse(status=400)
    except stripe.error.SignatureVerificationError as e:
        # Invalid signature
        return HttpResponse(status=400)

    # Handle the checkout.session.completed event
    if event['type'] == 'checkout.session.completed':
        session = event['data']['object']
        payment = Payments.objects.get(sessionId=session['id'])
        payment.status = 'Success'
        payment.save()

        Classified.objects.filter(pk=payment.itemId).update(is_active='Y', is_featured='Y', expires_on=datetime.now() + timedelta(days=30))
        print(session)
        line_items = stripe.checkout.Session.list_line_items(session['id'], limit=1)

    return HttpResponse(status=200)


