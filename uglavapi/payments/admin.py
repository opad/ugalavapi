from django.contrib import admin
from .models import Payments

# Register your models here.
@admin.register(Payments)
class PaymentsAdmin(admin.ModelAdmin):
    list_display = ("itemId","created_at", "status")
    date_hierarchy = 'created_at'