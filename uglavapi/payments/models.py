from django.db import models

# Create your models here.
# Create your models here.
class Payments(models.Model):
    sessionId = models.CharField(max_length=100, blank=False)
    itemId = models.CharField(max_length=100, blank=False)
    category = models.CharField(max_length=50)
    status = models.CharField(max_length=50)
    amount = models.DecimalField(decimal_places=2, max_digits=9)
    created_at = models.DateTimeField(auto_now_add=True)