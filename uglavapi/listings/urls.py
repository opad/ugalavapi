from django.urls import path, include
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework.routers import DefaultRouter
from listings import views

router = DefaultRouter()
router.register(r'api/listings', views.ListingViewSet)

urlpatterns = [
    path('', include(router.urls)),
]