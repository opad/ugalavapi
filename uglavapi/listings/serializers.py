from rest_framework import serializers
from listings.models import Listings, ListingCategory, Location
from django.contrib.auth.models import User

class ListingsSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')
    created = serializers.ReadOnlyField(source='created_at')
    #photo = serializers.ImageField(use_url=True, max_length=None,)

    class Meta:
        model = Listings
        fields = ['id','title', 'body', 'owner', 'category','contact','phone','website', 'email',
        'house','apt_no','zipcode','city','state', 'street','lat','longt','created']


class ListingCategorySerializer(serializers.ModelSerializer):
    icon = serializers.ImageField(use_url=True, max_length=None,)

    class Meta:
        model = ListingCategory
        fields = ['id','name','icon']

class LocationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Location
        fields = ['id','name']