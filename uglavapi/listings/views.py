from django.shortcuts import render
from rest_framework import filters
from django_filters.rest_framework import DjangoFilterBackend
from listings.models import Listings, ListingCategory, Location
from rest_framework import viewsets, permissions, pagination, generics
from .serializers import ListingsSerializer, ListingCategorySerializer, LocationSerializer
from listings.permissions import IsOwnerOrReadOnly


class ListingPagination(pagination.PageNumberPagination):
    page_size = 100

# Create your views here.
class ListingViewSet(viewsets.ModelViewSet):
    queryset = Listings.objects.all()
    permission_classes = [permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]
    serializer_class = ListingsSerializer
    pagination_class = ListingPagination
    search_fields = ['title','body', 'category', 'city','street','zipcode']
    filter_backends = (filters.SearchFilter,DjangoFilterBackend,)
    filterset_fields = ['location', 'category', 'state', 'city']

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


# Create your views here.
class ListingAdminViewSet(viewsets.ModelViewSet):
    queryset = Listings.objects.all()
    permission_classes = [permissions.IsAuthenticated, IsOwnerOrReadOnly]
    serializer_class = ListingsSerializer
    pagination_class = ListingPagination

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def get_queryset(self):
        return Listings.objects.filter(owner=self.request.user)


# Categories view here.
class ListingCategoryViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = ListingCategory.objects.all()
    permission_classes = [permissions.DjangoModelPermissionsOrAnonReadOnly]
    serializer_class = ListingCategorySerializer



# Categories view here.
class LocationViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Location.objects.all()
    permission_classes = [permissions.DjangoModelPermissionsOrAnonReadOnly]
    serializer_class = LocationSerializer
