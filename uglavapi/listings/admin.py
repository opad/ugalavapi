from django.contrib import admin
from django import forms
from .models import Listings, ListingCategory, Location


# Register your models here.
#admin.site.register(Listings)
class ListingForm(forms.ModelForm):
    MY_CATEGORIES = tuple(ListingCategory.objects.all().values_list('name', 'name'))
    MY_LOCATIONS  = tuple(Location.objects.all().values_list('name', 'name'))

    category = forms.ChoiceField(choices=MY_CATEGORIES)
    location = forms.ChoiceField(choices=MY_LOCATIONS)

@admin.register(Listings)
class ListingsAdmin(admin.ModelAdmin):
    list_display = ("title", "category","created_at")
    date_hierarchy = 'created_at'
    form = ListingForm


@admin.register(ListingCategory)
class ListingsAdmin(admin.ModelAdmin):
    list_display = ("name","created_at")
    date_hierarchy = 'created_at'


@admin.register(Location)
class ListingsAdmin(admin.ModelAdmin):
    list_display = ("name","created_at")
    date_hierarchy = 'created_at'