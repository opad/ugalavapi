from django.db import models

# Create your models here.
class Listings(models.Model):
    title = models.CharField(max_length=100, blank=False)
    body = models.TextField()
    category = models.CharField(max_length=50)
    contact = models.CharField(max_length=50)
    phone = models.CharField(max_length=100)
    website = models.CharField(max_length=255)
    email = models.CharField(max_length=100)
    location = models.CharField(max_length=100, blank=True, default='NONE')
    house = models.CharField(max_length=100, blank=True)
    apt_no = models.CharField(max_length=100, blank=True)
    street = models.CharField(max_length=65, blank=True)
    zipcode = models.CharField(max_length=12, blank=True)
    city = models.CharField(max_length=65, blank=True)
    state = models.CharField(max_length=100, blank=True)
    photo = models.ImageField(upload_to="uploads/images/", null=True, blank=True)
    lat = models.DecimalField(max_digits=8, decimal_places=5)
    longt = models.DecimalField(max_digits=8, decimal_places=5)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    owner = models.ForeignKey('auth.User', related_name='listings', on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = "Listings"
        ordering = ['title']

    def __str__(self):
        return self.title
    
 

class ListingCategory(models.Model):
    name = models.CharField(max_length=50)
    icon = models.ImageField(upload_to="uploads/images/icons/", null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = "Listing Categories"
        ordering = ['name']

    def __str__(self):
        return self.name


class Location(models.Model):
    name = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = "Locations"
        ordering = ['name']

    def __str__(self):
        return self.name