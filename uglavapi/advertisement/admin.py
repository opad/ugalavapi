from django.contrib import admin
from .models import Adverts

@admin.register(Adverts)
class AdvertsAdmin(admin.ModelAdmin):
    list_display = ("title", "phone","created_at", 'expires_on')
    date_hierarchy = 'expires_on'
