from django.shortcuts import render
from rest_framework import viewsets, permissions, pagination
from django.conf import settings
from django.urls import reverse
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Adverts
import stripe
from .serializer import AdvertSerializer
from payments.models import Payments
from datetime import datetime, timedelta

class AdvertPagination(pagination.LimitOffsetPagination):
    page_size = 1

class AdvertViewSet(viewsets.ModelViewSet):
    queryset = Adverts.objects.all()
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    serializer_class = AdvertSerializer
    pagination_class = AdvertPagination

    def get_queryset(self):
        return Adverts.objects.filter(is_active='Y').all()


@csrf_exempt
def advert_checkout(request):
    # get item in check for verification
    itemId = request.GET["itemId"]
    advert = Adverts.objects.filter(pk=itemId).first()
    product = request.GET["product"]
    price = 'price_1HoBP9CYHZJHJezoxin2Czwj'
    if product == '2':
        price = 'price_1HoBQHCYHZJHJezoPzTpD9wq'
        
    session = stripe.checkout.Session.create(
        payment_method_types=['card'],
        line_items=[{
            'price': price,
            'quantity': 1,
        }],
        mode='payment',
        success_url=request.build_absolute_uri(reverse('advert_thanks')) + '?session_id={CHECKOUT_SESSION_ID}',
        cancel_url=request.build_absolute_uri(reverse('advert_thanks')),
    )
    #add the payment to payments
    payment = Payments.objects.create(
        sessionId=session.id,
        itemId=request.GET["itemId"],
        amount=1.0,
        category='Advert',
        status='Pending'
    )

    return JsonResponse({
        'session_id' : session.id,
        'stripe_public_key' : settings.STRIPE_PUBLIC_KEY
    })


def advert_thanks(request):
    payment = Payments.objects.get(sessionId=request.GET['session_id'])

    payment.status = 'Success',
    payment.save()
    advert = Adverts.objects.filter(pk=payment.itemId).first()
    advert.is_active = 'Y'
    advert.expires_on = datetime.now() + timedelta(days=30)
    advert.save()
    return render(request, 'payments/thanks.html')

def advert_cancel(request):
    return render(request, 'payments/failed.html')


@csrf_exempt
def stripe_webhook(request):

    # You can find your endpoint's secret in your webhook settings
    endpoint_secret = 'whsec_Xj8wBk2qiUcjDEmYu5kfKkOrJCJ5UUjW'

    payload = request.body
    sig_header = request.META['HTTP_STRIPE_SIGNATURE']
    event = None

    try:
        event = stripe.Webhook.construct_event(
            payload, sig_header, endpoint_secret
        )
    except ValueError as e:
        # Invalid payload
        return HttpResponse(status=400)
    except stripe.error.SignatureVerificationError as e:
        # Invalid signature
        return HttpResponse(status=400)

    # Handle the checkout.session.completed event
    if event['type'] == 'checkout.session.completed':
        session = event['data']['object']
        payment = Payments.objects.get(sessionId=session['id'])
        payment.status = 'Success'
        payment.save()

        Adverts.objects.filter(pk=payment.itemId).update(is_active='Y', expires_on=datetime.now() + timedelta(days=30))
        print(session)
        line_items = stripe.checkout.Session.list_line_items(session['id'], limit=1)

    return HttpResponse(status=200)