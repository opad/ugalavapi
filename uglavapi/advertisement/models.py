from django.db import models

class Adverts(models.Model):
    title = models.CharField(max_length=200)
    phone = models.CharField(max_length=32)
    url = models.URLField(blank=True)
    photo = models.ImageField(upload_to="uploads/images/", null=True, blank=True)
    is_active = models.CharField(choices=(('N','NO'), ('Y', 'Yes')), max_length=32, default='N')
    expires_on = models.DateTimeField(blank=True, auto_now_add=True)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = "Adverts"
