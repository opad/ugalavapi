from rest_framework import serializers
from .models import Adverts


class AdvertSerializer(serializers.ModelSerializer):
    created = serializers.ReadOnlyField(source='created_at')
    photo = serializers.ImageField(use_url=True, max_length=None, required=False)

    class Meta:
        model = Adverts
        fields = ('id', 'title','photo', 'phone', 'url', 'created')