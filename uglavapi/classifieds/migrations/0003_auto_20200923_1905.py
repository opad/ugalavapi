# Generated by Django 3.1.1 on 2020-09-23 19:05

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('classifieds', '0002_auto_20200923_1904'),
    ]

    operations = [
        migrations.AlterField(
            model_name='categories',
            name='parent',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, related_name='sub_cats', to='classifieds.categories'),
        ),
    ]
