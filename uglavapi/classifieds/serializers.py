from rest_framework import serializers
from classifieds.models import Classified, Categories, ClassifiedsPhotos
from django.contrib.auth.models import User


class CategoriesSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')
    created = serializers.ReadOnlyField(source='created_at')

    class Meta:
        model = Categories
        fields = ['id','name','parent', 'owner', 'created']


class ClassifiedsSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')
    created = serializers.ReadOnlyField(source='created_at')
    photo = serializers.ImageField(use_url=True, max_length=None, required=False )

    class Meta:
        model = Classified
        fields = ['id', 'title','photo', 'detail','phone' ,'expected_price',
        'is_price_negotiable','owner', 'is_seller', 'created', 'is_active', 'is_featured', 'category',
        'is_individual','rate_period']


class PhotoSerializer(serializers.ModelSerializer):
    classified = serializers.ReadOnlyField(source='classified.id')
    class Meta:
        model = ClassifiedsPhotos
        fields = ['']
