from django.urls import path, include
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework.routers import DefaultRouter
from classifieds import views

router = DefaultRouter()
router.register(r'api/classifieds', views.ClassifiedViewSet)
router.register(r'api/categories', views.CategoriesViewSet)

urlpatterns = [
    path('', include(router.urls)),
]