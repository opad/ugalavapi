from django.shortcuts import render
from rest_framework import filters, generics

from django_filters.rest_framework import DjangoFilterBackend
from classifieds.models import Classified, Categories, ClassifiedsPhotos
from rest_framework import viewsets, permissions, pagination
from .serializers import ClassifiedsSerializer, CategoriesSerializer, PhotoSerializer
from listings.permissions import IsOwnerOrReadOnly
from rest_framework.decorators import api_view
import stripe

class ClassifiedPagination(pagination.LimitOffsetPagination):
    page_size = 100

class FeaturePagination(pagination.LimitOffsetPagination):
    page_size = 1

# Classified views here.
class ClassifiedViewSet(viewsets.ModelViewSet):
    queryset = Classified.objects.exclude(category=3).order_by('-created_at')
    permission_classes = [permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]
    search_fields = ['title','detail', 'is_featured', 'is_active', 'expected_price']
    filter_backends = (filters.SearchFilter,DjangoFilterBackend,)
    filterset_fields = ['is_featured', 'is_active']
    pagination_class = ClassifiedPagination
    serializer_class = ClassifiedsSerializer

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

# Classified views here.
class ClassifiedUserViewSet(viewsets.ModelViewSet):
    queryset = Classified.objects.all()
    permission_classes = [permissions.IsAuthenticated, IsOwnerOrReadOnly]
    pagination_class = ClassifiedPagination
    serializer_class = ClassifiedsSerializer

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def get_queryset(self):
        return Classified.objects.filter(owner=self.request.user)
    
    @classmethod
    def get_extra_actions(cls):
        return []

# Categories view here.
class CategoriesViewSet(viewsets.ModelViewSet):
    queryset = Categories.objects.all()
    permission_classes = [permissions.DjangoModelPermissionsOrAnonReadOnly, IsOwnerOrReadOnly]
    serializer_class = CategoriesSerializer

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class PhotosViewSet(viewsets.ModelViewSet):
    queryset = ClassifiedsPhotos.objects.all()
    permission_classes = [permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]
    serializer_class = PhotoSerializer


# Job Classified views here.
class JobsViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Classified.objects.filter(category=3).order_by('-created_at')
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    search_fields = ['title','detail', 'is_featured', 'is_active', 'expected_price']
    filter_backends = (filters.SearchFilter,DjangoFilterBackend,)
    filterset_fields = ['is_featured', 'is_active']
    pagination_class = ClassifiedPagination
    serializer_class = ClassifiedsSerializer

# Job Classified views here.
class FeaturedViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Classified.objects.filter(is_featured='Y').order_by('?')
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    pagination_class = FeaturePagination
    serializer_class = ClassifiedsSerializer