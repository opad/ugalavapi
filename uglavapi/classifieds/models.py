from django.db import models


class Categories(models.Model):
    name = models.CharField(max_length=50)
    parent = models.ForeignKey('self', related_name='sub_cats', on_delete=models.CASCADE, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    owner = models.ForeignKey('auth.User', related_name='categories', on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = "Categories"

    def __str__(self):
        return self.name

    
# Classifieds model.
class Classified(models.Model):
    title = models.CharField(max_length=200, blank=False)
    detail = models.TextField(blank=False)
    phone = models.CharField(max_length=200, blank=False, null=True)
    category = models.ForeignKey('Categories', related_name='classifieds', on_delete=models.CASCADE, blank=True)
    is_active = models.CharField(choices=(('Y','Yes'),('N', 'No')), default='N', max_length=12)
    is_seller = models.CharField(choices=(('Y','Yes'),('N', 'No')), max_length=12)
    is_individual = models.CharField(choices=(('Y','Yes'),('N', 'No')), max_length=12)
    expected_price = models.DecimalField(max_digits=12, decimal_places=2, default=0.0)
    rate_period = models.CharField(choices=(('Hour','Per Hour'),('Day', 'Per Day',),('Week', 'Per Week',),('Month','Per Month'),('Year', 'Per Year')), max_length=32, blank=True, null=True)
    is_price_negotiable = models.CharField(choices=(('Y','Yes'),('N', 'No')), max_length=12)
    created_at = models.DateTimeField(auto_now_add=True)
    last_renewed_on = models.DateTimeField(null=True)
    photo = models.ImageField(upload_to="uploads/images/", null=True, blank=True)
    is_featured = models.CharField(choices=(('Y','Yes'),('N', 'No')), default='N', max_length=12)
    expires_on = models.DateTimeField(null=True)
    owner = models.ForeignKey('auth.User', related_name='classifieds', on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = "Classifieds"
        ordering = ['title','created_at']

    def __str__(self):
        return self.title


class ClassifiedsPhotos(models.Model):
    classified = models.ForeignKey(Classified, on_delete=models.CASCADE)
    photo_url = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name
