from django.contrib import admin
from .models import Categories, Classified
# Register your models here.

#admin.site.register(Classified)
admin.site.register(Categories)

@admin.register(Classified)
class ClassifiedAdmin(admin.ModelAdmin):
    list_display = ("title", "phone","created_at")
    date_hierarchy = 'created_at'
