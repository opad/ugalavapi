from django.shortcuts import render
from rest_framework import viewsets
from rest_framework import views
from rest_framework.response import Response
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from .models import Profile
from django.contrib.auth.models import User
from .permissions import IsProfileOwnerOrReadOnly
from .serializers import ProfileSerializer, UserSerializer
from django.core.mail import send_mail


class ProfileListCreateView(ListCreateAPIView):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer
    permission_classes = [IsAuthenticated]

    def perform_create(self, serializer):
        user = self.request.user
        serializer.save(user=user)


class ProfileDetailView(RetrieveUpdateDestroyAPIView):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer
    permission_classes = [IsProfileOwnerOrReadOnly, IsAdminUser]


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes =[]


class SendMessageView(views.APIView):
    permission_classes = []

    def post(self, request, *args, **kwargs):
        email = request.POST.get('email', None)
        message = request.POST.get('comment', None)
        if email and message:
            message = f"Message:\n {message} \n from: {email}"
            print(message)
            send_mail('User Feedback', message, 'feedback@ugalav.com', ['jjyusuf@gmail.com'], fail_silently=True,)
            return Response({"success": True})
        else:
            return Response({"success": False})



