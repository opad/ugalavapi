from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from .models import Profile
from django.contrib.auth.models import User
from django.contrib.auth.models import Group

class ProfileSerializer(serializers.ModelSerializer):
    user = serializers.StringRelatedField(read_only=True)

    class Meta:
        model = Profile
        fields = '__all__'


class UserSerializer(serializers.ModelSerializer):
    #profile = ProfileSerializer()
    email = serializers.EmailField(
            required=True,
            validators=[UniqueValidator(queryset=User.objects.all())]
            )
    password = serializers.CharField(min_length=8, write_only=True)

    class Meta:
        model = User
        fields = ('username','email', 'password', 'first_name', 'last_name')
    
    def create(self, validated_data):
        #profile_data = validated_data.pop('profile')  

        my_group = Group.objects.get(name='Customer') 
        password = validated_data.pop('password')
        user = User(**validated_data)
        user.set_password(password)
        user.save()
        user.groups.add(my_group)
        #profile = Profile.objects.create(**profile_data, user=user)
        return user
