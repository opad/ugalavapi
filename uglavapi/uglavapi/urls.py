"""uglavapi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework.routers import DefaultRouter
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView
from classifieds.views import ClassifiedViewSet, CategoriesViewSet, JobsViewSet, FeaturedViewSet, ClassifiedUserViewSet
from listings.views import ListingViewSet, ListingAdminViewSet, ListingCategoryViewSet, LocationViewSet
from accounts.views import ProfileListCreateView, ProfileDetailView, UserViewSet, SendMessageView
from payments.views import checkout, thanks, stripe_webhook, cancel
from advertisement.views import AdvertViewSet, advert_cancel, advert_thanks, advert_checkout

router = DefaultRouter()
router.register(r'api/adverts', AdvertViewSet)
router.register(r'api/classifieds', ClassifiedViewSet)
router.register(r'api/user/classifieds', ClassifiedUserViewSet)
router.register(r'api/categories', CategoriesViewSet)
router.register(r'api/user/listings', ListingAdminViewSet)
router.register(r'api/listings', ListingViewSet)
router.register(r'api/listing-categories', ListingCategoryViewSet)
router.register(r'api/listing-locations', LocationViewSet)
router.register(r'api/users', UserViewSet)
router.register(r'api/jobs', JobsViewSet, basename='jobs')
router.register(r'api/featured', FeaturedViewSet, basename='featured')
#router.register(r'api/register/', UserViewSet)

admin.site.site_header = 'Ugalav Management Dashboard'                    # default: "Django Administration"
admin.site.index_title = 'Ugalav Admin Portal'                 # default: "Site administration"
admin.site.site_title = 'Welcome to Ugalav Admin Portal' # default: "Django site admin"

#fix routing issue by importing the router here for centralized routing
urlpatterns = [
    path('', include(router.urls)),
    # path('api/register/', UserViewSet),
    path('api/profiles/', ProfileListCreateView.as_view(), name='profiles'),
    path('api/profile/<int:pk>', ProfileDetailView.as_view(), name='profile'),
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh', TokenRefreshView.as_view(), name='token_refresh'),
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls')),
    path('auth/', include('rest_framework_social_oauth2.urls')),
    path('thanks/', thanks, name='thanks'),
    path('cancel/', cancel, name='cancel'),
    path('api/checkout/', checkout, name='checkout'),
    path('advert_thanks/', advert_thanks, name='advert_thanks'),
    path('advert_cancel/', advert_cancel, name='advert_cancel'),
    path('api/advert_checkout/', advert_checkout, name='advert_checkout'),
    path('api/stripe_webhook/', stripe_webhook, name='stripe_webhook'),
    path('api/feedback/', SendMessageView.as_view(), name='feedback')

]
